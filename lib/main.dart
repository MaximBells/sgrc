import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';
import 'package:sgrc/blocks/auth/screens/login_screen.dart';
import 'package:sgrc/blocks/main/data/main_controller.dart';
import 'package:sgrc/blocks/main/screens/main_block.dart';
import 'package:sgrc/blocks/profile/data/profile_controller.dart';
import 'package:sgrc/blocks/quick/data/quick_controller.dart';
import 'package:sgrc/blocks/quick/screens/quick_check_screen.dart';
import 'package:sgrc/blocks/quick/screens/quick_set_screen.dart';
import 'package:sgrc/components/screens/splash_screen/splash_screen.dart';
import 'package:sgrc/components/services/crypt_service.dart';
import 'package:sgrc/components/services/request_service.dart';
import 'package:sgrc/components/services/storage_service.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> initServices() async {
  Get.put(CryptService());
  await Get.putAsync(() => StorageService().init());
  await Supabase.initialize(
      url: 'https://wxmvdpotxvbzsowvgoov.supabase.co',
      anonKey:
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Ind4bXZkcG90eHZienNvd3Znb292Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2NzEwMzYwOTAsImV4cCI6MTk4NjYxMjA5MH0.429izLamye0t4kVfo6jj8f694DzHPN6U2WfhYMVT_nM');
  Get.put(Supabase.instance);
  Get.put(RequestService());
  Get.put(AuthController());
  Get.put(MainController());
  Get.put(ProfileController());
  Get.put(QuickController());
}

void main() async {
  await initServices();
  runApp(GetMaterialApp(
    theme: ThemeData.from(colorScheme: ThemeData.dark().colorScheme),
    defaultTransition: Transition.cupertino,
    debugShowCheckedModeBanner: false,
    transitionDuration: const Duration(milliseconds: 500),
    home: GestureDetector(
      onLongPress: () {
        Get.find<StorageService>().printAll();
      },
      child: const SplashScreen(),
    ),
  ));
}
/*
Get.find<StorageService>().isQuick
        ? const QuickCheckScreen()
        : const MainBlock()
 */
