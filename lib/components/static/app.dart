import 'package:get/get.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class App {
  static Supabase get supabase => Get.find<Supabase>();
}
