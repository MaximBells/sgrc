import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Utils {
  static void showLoader() {
    Navigator.push(
      Get.context!,
      PageRouteBuilder(
        pageBuilder: (_, __, ___) => const Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        ),
        transitionDuration: const Duration(seconds: 2),
        transitionsBuilder: (_, a, __, c) =>
            FadeTransition(opacity: a, child: c),
      ),
    );
  }

  static void closeLoader() {
    Navigator.pop(Get.context!);
  }
}
