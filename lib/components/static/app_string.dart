class AppString {
  static const enter = "Войти";
  static const enterSovcom = "Войти через аккаунт SovcomBank";
  static const or = "или";
  static const login = "Логин";
  static const password = "Пароль";
  static const loginHelperText = ""
      "Латинские буквы и цифры без пробелов";
  static const passwordHelperText = ""
      "Авторизуйтесь и начните общаться с трейдерами здесь и сейчас";
  static const ifNotHaveAccount = "Если у вас еще нет аккаунта";
  static const ifHaveAccount = "Если у вас есть аккаунт";
  static const authorise = "авторизуйтесь";
  static const ifNotRegister = "зарегистрируйтесь";
  static const register = "Зарегистрироваться";
}
