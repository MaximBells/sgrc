import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/components/screens/splash_screen/splash_screen_controller.dart';
import 'package:sgrc/components/static/app_image.dart';
import 'package:sgrc/components/widgets/mospolytech.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(SplashScreenController());
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const Expanded(flex: 1, child: SizedBox()),
            Expanded(
              flex: 3,
              child: Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 72, vertical: 36),
                child: Spin(
                  delay: const Duration(milliseconds: 500),
                  duration: const Duration(milliseconds: 2000),
                  infinite: true,
                  child: const Mospolytech(height: 300, width: 300),
                ),
              ),
            ),
            const Expanded(
                flex: 1,
                child: Text(
                  "Made by Mospolytech",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ))
          ],
        ),
      ),
    );
  }
}
