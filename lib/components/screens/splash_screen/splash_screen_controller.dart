import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/screens/auth_block.dart';
import 'package:sgrc/blocks/main/screens/main_block.dart';
import 'package:sgrc/blocks/profile/screens/profile_block.dart';
import 'package:sgrc/blocks/quick/screens/quick_check_screen.dart';
import 'package:sgrc/components/services/storage_service.dart';

class SplashScreenController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    Future.delayed(const Duration(seconds: 2)).then((value) {
      Get.offAll(() => Get.find<StorageService>().isQuick
          ? const QuickCheckScreen()
          : const AuthBlock());
    });
  }
}
