import 'package:get/get.dart';
import 'package:sgrc/components/services/crypt_service.dart';

extension StringExtension on String {
  String get md5 => Get.find<CryptService>().generateMd5(this);
}
