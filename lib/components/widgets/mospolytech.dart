import 'package:flutter/material.dart';
import 'package:sgrc/components/static/app_image.dart';

class Mospolytech extends StatelessWidget {
  const Mospolytech({Key? key, required this.height, required this.width})
      : super(key: key);
  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Image.asset(AppImage.logo),
    );
  }
}
