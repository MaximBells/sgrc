import 'dart:convert';

import 'package:get/get.dart';
import 'package:encrypt/encrypt.dart' as enc;
import 'package:crypto/crypto.dart';

class CryptValue {
  String password = "";
}

class CryptService extends GetxService {
  final _password = CryptValue().obs;

  String get password => _password.value.password;

  set password(String value) {
    _password.update((val) {
      val?.password = value;
    });
  }

  ///function to create hash
  String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

  ///encrypting function
  String encrypt(String text, String password) {
    final key = enc.Key.fromUtf8(generateMd5(password));
    final iv = enc.IV.fromLength(16);
    final encrypter = enc.Encrypter(enc.AES(key));
    final encrypted = encrypter.encrypt(text, iv: iv);
    return encrypted.base64;
  }

  ///decrypting function
  String decrypt(String text, String password) {
    final key = enc.Key.fromUtf8(generateMd5(password));
    final iv = enc.IV.fromLength(16);
    final encrypter = enc.Encrypter(enc.AES(key));
    final encrypted = enc.Encrypted.fromBase64(text);
    final content = encrypter.decrypt(encrypted, iv: iv);
    return content;
  }
}
