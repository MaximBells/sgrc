import 'dart:core';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:sgrc/components/enums/strorage_enum.dart';
import 'package:sgrc/components/extensions/string_extension.dart';
import 'package:sgrc/components/services/crypt_service.dart';

class StorageService extends GetxService {
  final storage = GetStorage();

  Future<StorageService> init() async {
    await GetStorage.init();
    return this;
  }

  String getEncryptedValue(StorageEnum storageEnum) {
    return Get.find<CryptService>().decrypt(
        storage.read(storageEnum.name.md5), Get.find<CryptService>().password);
  }

  dynamic getValue(StorageEnum storageEnum) =>
      storage.read(storageEnum.name.md5);

  void setPassword(String pinCode) {
    storage.write(
        StorageEnum.password.name.md5,
        Get.find<CryptService>()
            .encrypt(Get.find<CryptService>().password, pinCode.md5));
  }

  bool checkQuick(String pinCode) {
    try {
      String password = Get.find<CryptService>()
          .decrypt(getValue(StorageEnum.password), pinCode.md5);
      Get.find<CryptService>().password = password;
      return true;
    } catch (e) {
      return false;
    }
  }

  void setValue(StorageEnum storageEnum, dynamic value, {bool? needToEncrypt}) {
    if (needToEncrypt == true) {
      storage.write(
          storageEnum.name.md5,
          Get.find<CryptService>()
              .encrypt(value, Get.find<CryptService>().password));
    } else {
      storage.write(storageEnum.name.md5, value);
    }
  }

  Future<void> remove(StorageEnum storageEnum) async {
    await storage.remove(storageEnum.name.md5);
  }

  Future<void> removeAll() async {
    const uniqueNames = StorageEnum.values;
    for (var key in uniqueNames) {
      await storage.remove(key.name.md5).then((result) {
        print('removed ${key.name}');
      });
    }
    for (var key in uniqueNames) {
      print('${key.name} - ${storage.read(key.name.md5)}');
    }
  }

  void printAll() {
    const uniqueNames = StorageEnum.values;
    for (var key in uniqueNames) {
      print('${key.name} - ${storage.read(key.name.md5)}');
    }
  }

  bool get isQuick {
    if (getValue(StorageEnum.quickAuth) == null) {
      return false;
    } else {
      return true;
    }
  }

  bool get isAuth {
    if (getValue(StorageEnum.isAuth) == null) {
      return false;
    } else {
      return true;
    }
  }

  bool checkPassword({required String login, required String password}) {
    if (Get.find<CryptService>().encrypt(login, password) ==
        storage.read(StorageEnum.login.name.md5)) {
      Get.find<CryptService>().password = password;
      return true;
    } else {
      return false;
    }
  }
}
