import 'dart:convert';

import 'package:candlesticks/candlesticks.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';
import 'package:sgrc/blocks/main/data/main_controller.dart';
import 'package:sgrc/components/models/request_answer.dart';
import 'package:sgrc/components/static/app.dart';
import 'package:sgrc/components/static/utils.dart';

String host = "http://158.160.36.137";

class RequestService extends GetConnect {
  Future<List<Candle>> fetchCandles(int count, String symbol) async {
    final res = await get(
        "https://api.binance.com/api/v3/klines?symbol=$symbol&interval=1m&startTime=${DateTime.now().subtract(Duration(hours: count)).millisecondsSinceEpoch}");
    print(res.body);

    return (res.body as List<dynamic>)
        .map((e) => Candle.fromJson(e))
        .toList()
        .reversed
        .toList();
  }

  Future<bool> signUp(String login, String password) async {
    try {
      var result = await App.supabase.client.auth.signUp(login, password);
      if (result.error == null) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      Get.snackbar("Ошибка!", e.toString(),
          colorText: Get.theme.primaryColor, backgroundColor: Colors.yellow);
      return false;
    }
  }

  Future<RequestAnswer> signIn(String login, String password) async {
    try {
      var result = await App.supabase.client.auth
          .signIn(email: login, password: password);

      if (result.error == null) {
        return RequestAnswer(result: true);
      } else {
        Get.snackbar("Ошибка!", result.error?.message ?? "Some error!",
            colorText: Get.theme.primaryColor, backgroundColor: Colors.yellow);
        return RequestAnswer(result: false, errorMessage: result.error?.message);
      }
    } catch (e) {
      Get.snackbar("Ошибка!", e.toString(),
          colorText: Get.theme.primaryColor, backgroundColor: Colors.yellow);
      return RequestAnswer(result: false, errorMessage: e.toString());
    }
  }

  Future<void> getTradingTools() async {
    final res = await get("$host/api/trading_tools/");
    print(res.body);
  }
}
