class TradingTool {
  final String name;
  final String? code;
  final String type;

  TradingTool({required this.name, this.code, required this.type});
}
