class User {
  final String email;
  final String passwordHash;
  final String firstName;
  final String lastName;
  final String? middleName;
  final String? phoneNumber;

  User(
      {required this.email,
      required this.passwordHash,
      required this.firstName,
      required this.lastName,
      this.middleName,
      this.phoneNumber});
}
