import 'package:sgrc/components/models/account.dart';
import 'package:sgrc/components/models/trading_tool.dart';

class Amount {
  final int tradingToolId;
  final TradingTool tradingTool;
  final int accountId;
  final Account account;
  final double amount;

  Amount({
    required this.tradingToolId,
    required this.tradingTool,
    required this.accountId,
    required this.account,
    required this.amount,
  });
}
