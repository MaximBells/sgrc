import 'package:sgrc/components/enums/account_status.dart';
import 'package:sgrc/components/models/currency.dart';
import 'package:sgrc/components/models/user.dart';

class Account {
  final double amount;
  final int currencyId;
  final Currency currency;
  final DateTime dttmOpened;
  final DateTime? dttmClosed;
  final AccountStatus status;
  final String registrationNumber;
  final String? name;
  final bool isDemo;
  final int userId;
  final User user;

  Account(
      {this.amount = 0,
      required this.currencyId,
      required this.currency,
      required this.dttmOpened,
      this.dttmClosed,
      this.status = AccountStatus.active,
      required this.registrationNumber,
      this.name,
      this.isDemo = false,
      required this.userId,
      required this.user});
}
