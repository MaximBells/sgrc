import 'package:sgrc/components/models/position.dart';

class Transaction {
  final DateTime dttm;
  final double price;
  final int positionId;
  final Position position;

  Transaction(
      {required this.dttm,
      required this.price,
      required this.positionId,
      required this.position});
}
