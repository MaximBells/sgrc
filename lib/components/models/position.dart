import 'package:sgrc/components/enums/position_status.dart';
import 'package:sgrc/components/models/account.dart';
import 'package:sgrc/components/models/trading_tool.dart';

class Position {
  final String positionType;
  final DateTime dttmCreated;
  final DateTime? dttmTransaction;
  final DateTime? dttmDeferredClose;
  final double amount;
  final double price;
  final PositionStatus status;
  final int accountId;
  final Account account;
  final int tradingToolId;
  final TradingTool tradingTool;

  Position(
      {required this.positionType,
      required this.dttmCreated,
      this.dttmTransaction,
      this.dttmDeferredClose,
      required this.amount,
      required this.price,
      this.status = PositionStatus.planned,
      required this.accountId,
      required this.account,
      required this.tradingToolId,
      required this.tradingTool});
}
