class RequestAnswer {
  bool result;
  String? errorMessage;

  RequestAnswer({required this.result, this.errorMessage});
}
