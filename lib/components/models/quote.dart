import 'package:sgrc/components/models/trading_tool.dart';

class Quote {
  final DateTime dttm;
  final double value;
  final int tradingToolId;
  final TradingTool tradingTool;

  Quote(
      {required this.dttm,
      required this.value,
      required this.tradingToolId,
      required this.tradingTool});
}
