import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/screens/auth_block.dart';

import 'package:sgrc/blocks/main/screens/main_block.dart';
import 'package:sgrc/blocks/profile/data/profile_controller.dart';
import 'package:sgrc/blocks/quick/data/quick_controller.dart';
import 'package:sgrc/blocks/quick/widgets/circle_widget/circle_widget.dart';
import 'package:sgrc/components/enums/quick_status.dart';
import 'package:sgrc/components/services/storage_service.dart';
import 'package:sgrc/components/static/app_image.dart';
import 'package:sgrc/components/widgets/mospolytech.dart';

class QuickCheckScreen extends StatelessWidget {
  const QuickCheckScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(QuickController());
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.find<StorageService>().removeAll();
          Get.find<ProfileController>().isAuth = false;
          Get.off(() => const AuthBlock());
        },
        child: Icon(Icons.exit_to_app),
      ),
      body: SafeArea(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 48),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 64),
                child: Spin(
                  delay: const Duration(milliseconds: 500),
                  duration: const Duration(milliseconds: 3000),
                  infinite: true,
                  child: const Mospolytech(height: 150, width: 150),
                ),
              ),
              const Text(
                "Введите пин-код для авторизации",
                style: TextStyle(fontSize: 16),
              ),
              Container(
                  margin: const EdgeInsets.symmetric(vertical: 24),
                  child: const CircleWidget(
                    quickStatus: QuickStatus.check,
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
