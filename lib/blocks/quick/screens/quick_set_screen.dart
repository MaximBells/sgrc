import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:sgrc/blocks/quick/widgets/circle_widget/circle_controller.dart';
import 'package:sgrc/blocks/quick/widgets/keyboard_widget.dart';
import 'package:sgrc/blocks/quick/widgets/circle_widget/circle_widget.dart';
import 'package:sgrc/components/enums/quick_status.dart';
import 'package:sgrc/components/static/app_image.dart';
import 'package:sgrc/components/widgets/mospolytech.dart';

class QuickSetScreen extends StatelessWidget {
  const QuickSetScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 48),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 64),
                child: Spin(
                  delay: const Duration(milliseconds: 500),
                  duration: const Duration(milliseconds: 3000),
                  infinite: true,
                  child: const Mospolytech(height: 150, width: 150),
                ),
              ),
              Text(
                "Создайте пин-код для быстрого входа",
                style: TextStyle(fontSize: 16),
              ),
              Container(
                  margin: const EdgeInsets.symmetric(vertical: 24),
                  child: CircleWidget(
                    quickStatus: QuickStatus.set,
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
