import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/quick/widgets/circle_widget/circle_controller.dart';

class KeyBoardWidget extends StatefulWidget {
  const KeyBoardWidget({Key? key, required this.circleController}) : super(key: key);
  final CircleController circleController;

  @override
  State<KeyBoardWidget> createState() => _KeyBoardWidgetState();
}

class _KeyBoardWidgetState extends State<KeyBoardWidget> {
  String pinCode = "";

  @override
  Widget build(BuildContext context) {
    return keyboard();
  }

  Widget keyboard() {
    return Column(
      children: <Widget>[
        oneLineOfButtons([1, 2, 3]),
        oneLineOfButtons([4, 5, 6]),
        oneLineOfButtons([7, 8, 9]),
        Row(
          children: <Widget>[
            const Spacer(),
            ButtonTheme(
              minWidth: 68.0,
              height: 68.0,
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.transparent),
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.transparent),
                  overlayColor:
                      MaterialStateProperty.all<Color>(Colors.transparent),
                ),
                onPressed: () {},
                child: const SizedBox(height: 68),
              ),
            ),
            const SizedBox(width: 42),
            numberButton(0),
            const SizedBox(width: 42),
            ButtonTheme(
              minWidth: 68.0,
              height: 68.0,
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.transparent),
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.transparent),
                  overlayColor:
                      MaterialStateProperty.all<Color>(Colors.transparent),
                ),
                onPressed: () {
                  if (pinCode.isNotEmpty) {
                    pinCode = pinCode.substring(0, pinCode.length - 1);
                    widget.circleController.updateValue(pinCode);
                  }
                },
                child: const Icon(
                  Icons.backspace_outlined,
                  color: Colors.white,
                  size: 30,
                ),
              ),
            ),
            const Spacer(),
          ],
        ),
      ],
    );
  }

  Container numberButton(int number) {
    return Container(
      margin: EdgeInsets.only(right: number % 3 != 0 ? 42 : 0),
      child: ButtonTheme(
        minWidth: 68.0,
        height: 68.0,
        child: TextButton(
          style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all<Color>(Colors.transparent),
            foregroundColor:
                MaterialStateProperty.all<Color>(Colors.transparent),
            overlayColor: MaterialStateProperty.all<Color>(Colors.transparent),
          ),
          onPressed: () {
            if (pinCode.length < 4) {
              pinCode += number.toString();
              widget.circleController.updateValue(pinCode);

              if (pinCode.length == 4) {
                pinCode = "";
              }
            }
            print(pinCode);
          },
          child: Text(
            number.toString(),
            style: const TextStyle(
              color: Colors.white,
              fontSize: 40.0,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ),
    );
  }

  Column oneLineOfButtons(List<int> values) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            const Spacer(),
            for (var value in values)
              numberButton(
                value,
              ),
            const Spacer(),
          ],
        ),
        const SizedBox(height: 32)
      ],
    );
  }
}
