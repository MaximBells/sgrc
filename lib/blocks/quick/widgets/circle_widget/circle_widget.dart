import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:sgrc/blocks/quick/widgets/circle_widget/circle_controller.dart';
import 'package:sgrc/components/enums/quick_status.dart';

class CircleWidget extends StatelessWidget {
  const CircleWidget({Key? key, required this.quickStatus}) : super(key: key);
  final QuickStatus quickStatus;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CircleController>(
        init: CircleController(quickStatus: quickStatus),
        global: false,
        builder: (controller) {
          return PinCodeTextField(
            length: 6,
            animationType: AnimationType.fade,
            keyboardType: TextInputType.number,
            pinTheme: PinTheme(
              shape: PinCodeFieldShape.box,
              borderRadius: BorderRadius.circular(5),
              fieldHeight: 50,
              fieldWidth: 40,
              selectedFillColor: Colors.transparent,
              selectedColor: Colors.cyan,
              activeFillColor: Colors.cyan,
              inactiveFillColor: Colors.transparent,
              disabledColor: Colors.cyan,
              activeColor: Colors.cyan,
              inactiveColor: Colors.cyan,
            ),
            autoFocus: true,
            cursorColor: Colors.white,
            controller: controller.textController,
            animationDuration: Duration(milliseconds: 300),
            backgroundColor: Colors.transparent,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            enableActiveFill: true,
            onCompleted: (v) {
              print("Completed");
            },
            onChanged: (value) {
              controller.updateValue(value);
            },
            beforeTextPaste: (text) {
              print("Allowing to paste $text");
              //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
              //but you can show anything you want here, like your pop up saying wrong paste format or etc
              return true;
            },
            appContext: context,
          );
        });
  }
}
