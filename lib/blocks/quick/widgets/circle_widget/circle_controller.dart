import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';
import 'package:sgrc/blocks/main/screens/main_block.dart';
import 'package:sgrc/blocks/profile/data/profile_controller.dart';
import 'package:sgrc/blocks/quick/data/quick_controller.dart';
import 'package:sgrc/blocks/quick/screens/quick_check_screen.dart';
import 'package:sgrc/blocks/quick/screens/quick_confirm_screen.dart';
import 'package:sgrc/blocks/quick/screens/quick_set_screen.dart';
import 'package:sgrc/components/enums/quick_status.dart';
import 'package:sgrc/components/enums/strorage_enum.dart';
import 'package:sgrc/components/services/crypt_service.dart';
import 'package:sgrc/components/services/request_service.dart';
import 'package:sgrc/components/services/storage_service.dart';

class CircleValue {
  String text = "";
}

class CircleController extends GetxController {
  final TextEditingController textController = TextEditingController();

  final QuickStatus quickStatus;

  CircleController({required this.quickStatus});

  final _circleValue = CircleValue().obs;

  String get text => _circleValue.value.text;

  set text(String value) {
    _circleValue.update((val) {
      val?.text = value;
    });
    update();
  }

  void updateValue(String value) {
    text = value;
    if (text.length == 6 && quickStatus == QuickStatus.set) {
      Get.find<QuickController>().prePin = text;
      Get.off(() => QuickConfirmScreen());
    } else if (text.length == 6 &&
        quickStatus == QuickStatus.confirm &&
        text != Get.find<QuickController>().prePin) {
      Get.snackbar("Пин-коды не совпадают!", "Пин-коды должны совпадать",
          backgroundColor: Colors.yellow, colorText: Get.theme.primaryColor);
      Get.find<QuickController>().prePin = "";
      Future.delayed(const Duration(seconds: 2))
          .then((value) => Get.off(() => const QuickSetScreen()));
    } else if (text.length == 6 &&
        quickStatus == QuickStatus.confirm &&
        text == Get.find<QuickController>().prePin) {
      Get.find<StorageService>().setValue(StorageEnum.quickAuth, true);
      Get.find<StorageService>().setPassword(text);
      Get.snackbar("Пин-код успешно создан!", "Успех!",
          backgroundColor: Colors.green, colorText: Get.theme.primaryColor);
      Future.delayed(const Duration(seconds: 2))
          .then((value) => Get.off(() => const MainBlock()));
    } else if (text.length == 6 &&
        quickStatus == QuickStatus.check &&
        Get.find<StorageService>().checkQuick(text) == true) {
      Get.snackbar("Пин-код верный!", "Успех!",
          backgroundColor: Colors.green, colorText: Get.theme.primaryColor);
      Get.find<AuthController>().login =
          Get.find<StorageService>().getEncryptedValue(StorageEnum.login);
      Get.find<RequestService>().signIn(
          Get.find<AuthController>().login,
          Get.find<StorageService>()
              .getEncryptedValue(StorageEnum.loginPassword));
      Get.find<ProfileController>().isAuth = true;
      Future.delayed(const Duration(seconds: 2))
          .then((value) => Get.off(() => const MainBlock()));
    } else if (text.length == 6 &&
        quickStatus == QuickStatus.check &&
        Get.find<StorageService>().checkQuick(text) == false) {
      textController.text = "";
      text = textController.text;
      Get.snackbar("Пин-код не совпадает!", "Пин-код должен совпадать",
          backgroundColor: Colors.yellow, colorText: Get.theme.primaryColor);
      Future.delayed(const Duration(seconds: 3)).then((value) {
        Get.find<QuickController>().wrong++;
      });
    }
    update();
  }
}
