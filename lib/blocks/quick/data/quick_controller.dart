import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/main/screens/main_block.dart';
import 'package:sgrc/blocks/profile/data/profile_controller.dart';
import 'package:sgrc/blocks/quick/screens/quick_check_screen.dart';
import 'package:sgrc/components/services/storage_service.dart';

class QuickValue {
  String pinCode = "";
  String prePin = "";
  int wrong = 0;
}

class QuickController extends GetxController {
  final _quickValue = QuickValue().obs;

  String get pinCode => _quickValue.value.pinCode;

  set pinCode(String value) {
    _quickValue.update((val) {
      val?.pinCode = value;
    });
  }

  String get prePin => _quickValue.value.prePin;

  set prePin(String value) {
    _quickValue.update((val) {
      val?.prePin = value;
    });
  }

  int get wrong => _quickValue.value.wrong;

  set wrong(int value) {
    _quickValue.update((val) {
      val?.wrong = value;
    });
    if (wrong == 3) {
      Get.find<StorageService>().removeAll();
      Get.find<ProfileController>().isAuth = false;
      Get.off(() => const MainBlock());
    } else {
      Navigator.pushReplacement(Get.context!,
          MaterialPageRoute(builder: (context) => const QuickCheckScreen()));
    }
  }
}
