import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

class UploadFile extends StatefulWidget {
  const UploadFile({Key? key, required this.label}) : super(key: key);
  final String label;

  @override
  State<UploadFile> createState() => _UploadFileState();
}

class _UploadFileState extends State<UploadFile> {
  final TextEditingController textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 32, vertical: 24),
      width: MediaQuery.of(context).size.width * 0.65,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              margin: const EdgeInsets.only(bottom: 12),
              child: Text(
                widget.label,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              )),
          TextField(
            controller: textEditingController,
            enabled: true,
            readOnly: true,
            style: const TextStyle(color: Colors.black, fontSize: 18),
            onTap: () async {
              print('tap');
              FilePickerResult? result = await FilePicker.platform.pickFiles();
              if (result != null) {
                setState(() {
                  textEditingController.text = result.files.single.name;
                });
              }
            },
            decoration: const InputDecoration(
                suffixIcon: Icon(
                  Icons.add_box_outlined,
                  color: Colors.cyan,
                ),
                filled: true,
                hintText: "Не заполнено",
                hintStyle: TextStyle(color: Colors.grey),
                fillColor: Colors.white,
                border: OutlineInputBorder()),
          )
        ],
      ),
    );
  }
}
