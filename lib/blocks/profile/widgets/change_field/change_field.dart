import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/profile/widgets/change_field/change_field_controller.dart';

class ChangeField extends StatelessWidget {
  const ChangeField({Key? key, required this.label, required this.inside})
      : super(key: key);
  final String label;
  final String inside;

  @override
  Widget build(BuildContext context) {
    if (inside == 'Не заполнено') {
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 32, vertical: 24),
        width: MediaQuery.of(context).size.width * 0.65,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                margin: const EdgeInsets.only(bottom: 12),
                child: Text(
                  label,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 15),
                )),
            TextField(
              enabled: true,
              style: const TextStyle(color: Colors.black, fontSize: 18),
              decoration: InputDecoration(
                  filled: true,
                  hintText: inside,
                  hintStyle: TextStyle(color: Colors.grey),
                  fillColor: Colors.white,
                  border: const OutlineInputBorder()),
            )
          ],
        ),
      );
    } else {
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 32, vertical: 24),
        width: MediaQuery.of(context).size.width * 0.65,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                margin: const EdgeInsets.only(bottom: 12),
                child: Text(
                  label,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                )),
            GetBuilder<ChangeFieldController>(
                global: false,
                init: ChangeFieldController(inside: inside),
                builder: (controller) {
                  return TextField(
                    controller: controller.textEditingController,
                    style: TextStyle(color: Colors.black, fontSize: 18),
                    decoration: const InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: const OutlineInputBorder()),
                  );
                })
          ],
        ),
      );
    }
  }
}
