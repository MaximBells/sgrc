import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChangeFieldController extends GetxController {
  final TextEditingController textEditingController = TextEditingController();

  final String inside;

  ChangeFieldController({required this.inside});

  @override
  void onInit() {
    super.onInit();

    textEditingController.text = inside;
  }
}
