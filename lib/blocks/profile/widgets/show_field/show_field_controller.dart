import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShowFieldController extends GetxController {
  final String inside;

  ShowFieldController({required this.inside});

  final TextEditingController textEditingController = TextEditingController();

  @override
  void onInit() {
    super.onInit();
    textEditingController.text = inside;
  }
}
