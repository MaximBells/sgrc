import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/profile/widgets/show_field/show_field_controller.dart';

class ShowField extends StatelessWidget {
  const ShowField({Key? key, required this.label, required this.inside})
      : super(key: key);
  final String label;
  final String inside;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 32, vertical: 24),
      width: MediaQuery.of(context).size.width * 0.65,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              margin: const EdgeInsets.only(bottom: 12),
              child: Text(
                label,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              )),
          GetBuilder<ShowFieldController>(
              global: false,
              init: ShowFieldController(inside: inside),
              builder: (controller) {
                return TextField(
                  controller: controller.textEditingController,
                  enabled: false,
                  style: TextStyle(color: Colors.black, fontSize: 18),
                  decoration: const InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      border: const OutlineInputBorder()),
                );
              })
        ],
      ),
    );
  }
}
