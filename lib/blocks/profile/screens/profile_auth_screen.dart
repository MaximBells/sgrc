import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';
import 'package:sgrc/blocks/auth/screens/auth_block.dart';
import 'package:sgrc/blocks/main/screens/main_block.dart';
import 'package:sgrc/blocks/profile/data/profile_controller.dart';
import 'package:sgrc/blocks/profile/screens/profile_change.dart';
import 'package:sgrc/blocks/profile/screens/profile_change_block.dart';
import 'package:sgrc/blocks/profile/widgets/show_field/show_field.dart';
import 'package:sgrc/components/services/storage_service.dart';

class ProfileAuthScreen extends GetView<ProfileController> {
  const ProfileAuthScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () {
                Get.find<StorageService>().removeAll();
                Get.find<ProfileController>().isAuth = false;
                Get.off(() => const AuthBlock());
              },
              icon: const Icon(Icons.exit_to_app))
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     Get.to(() => const ProfileChangeBlock());
      //   },
      //   child: const Icon(Icons.edit),
      // ),
      body: SafeArea(
        child: ListView(
          children: [
            Obx(
              () => ShowField(
                  label: "Логин", inside: Get.find<AuthController>().login),
            ),
            Obx(
              () => ShowField(
                  label: "Имя",
                  inside: Get.find<ProfileController>().firstName),
            ),
            Obx(
              () => ShowField(
                  label: "Фамилия",
                  inside: Get.find<ProfileController>().lastName),
            ),
            Obx(
              () => ShowField(
                  label: "Отчество",
                  inside: Get.find<ProfileController>().middleName),
            ),
            Obx(
              () => ShowField(
                  label: "Номер телефона",
                  inside: Get.find<ProfileController>().phoneNumber),
            ),
            Obx(
              () => ShowField(
                  label: "Паспорт",
                  inside: Get.find<ProfileController>().passport),
            )
          ],
        ),
      ),
    );
  }
}
