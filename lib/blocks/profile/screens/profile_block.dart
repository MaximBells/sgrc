import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/profile/data/profile_controller.dart';

class ProfileBlock extends GetView<ProfileController> {
  const ProfileBlock({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => controller.activeScreen);
  }
}
