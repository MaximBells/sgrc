import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/components/widgets/button.dart';

class ProfileOnVerify extends StatelessWidget {
  const ProfileOnVerify({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text(
            "Документы отправлены на верификацию",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 32, vertical: 24),
            width: MediaQuery.of(context).size.width * 0.65,
            child: Button(
                child: const Text("Закрыть"),
                onPressed: () {
                  Get.back();
                }),
          )
        ],
      ),
    );
  }
}
