import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/profile/data/profile_controller.dart';

class ProfileChangeBlock extends GetView<ProfileController> {
  const ProfileChangeBlock({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => AnimatedSwitcher(
          duration: const Duration(milliseconds: 500),
          child: controller.profileChangeBlock,
        ));
  }
}
