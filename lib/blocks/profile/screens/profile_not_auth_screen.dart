import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';
import 'package:sgrc/blocks/auth/screens/login_screen.dart';
import 'package:sgrc/blocks/auth/screens/registration_screen.dart';
import 'package:sgrc/components/enums/auth_type.dart';
import 'package:sgrc/components/widgets/button.dart';

class ProfileNotAuthScreen extends StatelessWidget {
  const ProfileNotAuthScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text("Вы не авторизованы"),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 20),
              child: Button(
                  child: const Text("Вход"),
                  onPressed: () {
                    Get.find<AuthController>().openAuth(AuthType.login);
                  }),
            ),
            Button(
                backgroundColor: Colors.white,
                child: const Text(
                  "Регистрация",
                  style: TextStyle(color: Colors.black),
                ),
                onPressed: () {
                  Get.find<AuthController>().openAuth(AuthType.register);
                }),
          ],
        ),
      ),
    );
  }
}
