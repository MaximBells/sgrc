import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/profile/data/profile_controller.dart';
import 'package:sgrc/blocks/profile/screens/profile_on_verify.dart';
import 'package:sgrc/blocks/profile/widgets/change_field/change_field.dart';
import 'package:sgrc/blocks/profile/widgets/upload_file.dart';
import 'package:sgrc/components/enums/profile_status.dart';
import 'package:sgrc/components/enums/strorage_enum.dart';
import 'package:sgrc/components/services/storage_service.dart';
import 'package:sgrc/components/widgets/button.dart';

class ProfileChange extends StatelessWidget {
  const ProfileChange({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: [
            Obx(
              () => ChangeField(
                  label: "Имя",
                  inside: Get.find<ProfileController>().firstName),
            ),
            Obx(
              () => ChangeField(
                  label: "Фамилия",
                  inside: Get.find<ProfileController>().lastName),
            ),
            Obx(
              () => ChangeField(
                  label: "Отчество",
                  inside: Get.find<ProfileController>().middleName),
            ),
            Obx(
              () => ChangeField(
                  label: "Номер телефона",
                  inside: Get.find<ProfileController>().phoneNumber),
            ),
            const UploadFile(
              label: "Паспорт",
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 32, vertical: 24),
              width: MediaQuery.of(context).size.width * 0.65,
              child: Button(
                  child: const Text("Отправить на верификацию"),
                  onPressed: () {
                    Get.find<StorageService>().setValue(
                        StorageEnum.profileStatus,
                        ProfileStatus.onVerification.name);
                    Get.find<ProfileController>().profileChangeBlock =
                        const ProfileOnVerify();
                  }),
            )
          ],
        ),
      ),
    );
  }
}
