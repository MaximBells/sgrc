import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/profile/screens/profile_auth_screen.dart';
import 'package:sgrc/blocks/profile/screens/profile_change.dart';
import 'package:sgrc/blocks/profile/screens/profile_not_auth_screen.dart';
import 'package:sgrc/blocks/profile/screens/profile_on_verify.dart';
import 'package:sgrc/components/enums/profile_status.dart';
import 'package:sgrc/components/enums/strorage_enum.dart';
import 'package:sgrc/components/services/storage_service.dart';

class ProfileValue {
  bool isAuth = false;
  Widget activeScreen = const ProfileAuthScreen();
  String firstName = "Не заполнено";
  String lastName = "Не заполнено";
  String middleName = "Не заполнено";
  String phoneNumber = "Не заполнено";
  String passport = "Не заполнено";
  ProfileStatus profileStatus = ProfileStatus.unverified;
  Widget profileChangeBlock = const ProfileChange();
}

class ProfileController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    isAuth = Get.find<StorageService>().isAuth;

    profileStatus = profileStatusFromString(
        Get.find<StorageService>().getValue(StorageEnum.profileStatus) ??
            ProfileStatus.unverified.name);
    if (profileStatus == ProfileStatus.onVerification) {
      profileChangeBlock = const ProfileOnVerify();
    }
  }

  final _profileValue = ProfileValue().obs;

  bool get isAuth => _profileValue.value.isAuth;

  set isAuth(bool value) {
    _profileValue.update((val) {
      val?.isAuth = value;
    });
  }

  Widget get activeScreen => _profileValue.value.activeScreen;

  set activeScreen(Widget value) {
    _profileValue.update((val) {
      val?.activeScreen = value;
    });
  }

  String get firstName => _profileValue.value.firstName;

  set firstName(String value) {
    _profileValue.update((val) {
      val?.firstName = value;
    });
  }

  String get lastName => _profileValue.value.lastName;

  set lastName(String value) {
    _profileValue.update((val) {
      val?.lastName = value;
    });
  }

  String get middleName => _profileValue.value.middleName;

  set middleName(String value) {
    _profileValue.update((val) {
      val?.middleName = value;
    });
  }

  String get phoneNumber => _profileValue.value.phoneNumber;

  set phoneNumber(String value) {
    _profileValue.update((val) {
      val?.phoneNumber = value;
    });
  }

  String get passport => _profileValue.value.passport;

  set passport(String value) {
    _profileValue.update((val) {
      val?.passport = value;
    });
  }

  ProfileStatus get profileStatus => _profileValue.value.profileStatus;

  set profileStatus(ProfileStatus value) {
    _profileValue.update((val) {
      val?.profileStatus = value;
    });
  }

  Widget get profileChangeBlock => _profileValue.value.profileChangeBlock;

  set profileChangeBlock(Widget value) {
    _profileValue.update((val) {
      val?.profileChangeBlock = value;
    });
  }

  ProfileStatus profileStatusFromString(String name) {
    ProfileStatus result = ProfileStatus.unverified;
    for (var value in ProfileStatus.values) {
      if (value.name.contains(name)) {
        result = value;
      }
    }
    return result;
  }
}
