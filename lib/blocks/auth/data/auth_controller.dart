import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/screens/auth_block.dart';
import 'package:sgrc/blocks/auth/screens/confirm_screen.dart';
import 'package:sgrc/blocks/auth/screens/login_screen.dart';
import 'package:sgrc/blocks/auth/screens/registration_screen.dart';
import 'package:sgrc/blocks/main/screens/main_block.dart';
import 'package:sgrc/blocks/profile/data/profile_controller.dart';
import 'package:sgrc/blocks/quick/data/quick_controller.dart';
import 'package:sgrc/blocks/quick/screens/quick_check_screen.dart';
import 'package:sgrc/blocks/quick/screens/quick_set_screen.dart';
import 'package:sgrc/components/enums/auth_type.dart';
import 'package:sgrc/components/enums/strorage_enum.dart';
import 'package:sgrc/components/extensions/string_extension.dart';
import 'package:sgrc/components/services/crypt_service.dart';
import 'package:sgrc/components/services/request_service.dart';
import 'package:sgrc/components/services/storage_service.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:sgrc/components/static/app.dart';
import 'package:sgrc/components/static/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class AuthValue {
  String login = "";
  String password = "";
  String createLogin = "";
  String createPassword1 = "";
  String createPassword2 = "";
  bool loginError = true;
  bool passwordError = true;
  Widget activeScreen = const LoginScreen();
  bool loginAuthError = true;
  bool passwordAuthError = true;
  String accessToken = "";
  String activeTradingTool = "";
}

class AuthController extends GetxController {
  final _authValue = AuthValue().obs;

  Widget get activeScreen => _authValue.value.activeScreen;

  @override
  void onInit() {
    super.onInit();
    // login = Get.find<StorageService>().getValue(StorageEnum.login);
  }

  Future<bool> confirm() async {
    Utils.showLoader();
    try {
      // final GotrueUserResponse updatedUser =
      //     await App.supabase.client.auth.update(
      //   UserAttributes(
      //     email: login,
      //   ),
      // );
      // print(updatedUser.error);
      // if (updatedUser.user?.emailConfirmedAt == null) {
      //   result = false;
      // }
      var result = await Get.find<RequestService>().signIn(login, password);
      Utils.closeLoader();
      if (result.result == true) {
        Get.find<CryptService>().password = password;
        Get.find<ProfileController>().isAuth = true;
        Get.find<StorageService>()
            .setValue(StorageEnum.login, login, needToEncrypt: true);
        Get.find<StorageService>().setValue(StorageEnum.isAuth, true);
        Get.off(() => const QuickSetScreen());
      }
      return result.result;
    } catch (e) {
      Utils.closeLoader();
      Get.snackbar(e.toString(), "Ошибка!",
          backgroundColor: Colors.yellow, colorText: Get.theme.primaryColor);
      return false;
    }
  }

  String get activeTradingTool => _authValue.value.activeTradingTool;

  set activeTradingTool(String value) {
    _authValue.update((val) {
      val?.activeTradingTool = value;
    });
  }

  set activeScreen(Widget value) {
    _authValue.update((val) {
      val?.activeScreen = value;
    });
  }

  void openAuth(AuthType authType) {
    switch (authType) {
      case AuthType.login:
        activeScreen = const LoginScreen();
        break;
      case AuthType.register:
        activeScreen = const RegistrationScreen();
        break;
    }
    Get.to(() => const AuthBlock());
  }

  void changeAuth(AuthType authType) {
    switch (authType) {
      case AuthType.login:
        activeScreen = const LoginScreen();
        break;
      case AuthType.register:
        activeScreen = const RegistrationScreen();
        break;
    }
  }

  String get login => _authValue.value.login;

  set login(String value) {
    _authValue.update((val) {
      val?.login = value;
    });
  }

  String get password => _authValue.value.password;

  set password(String value) {
    _authValue.update((val) {
      val?.password = value;
    });
  }

  String get createLogin => _authValue.value.createLogin;

  set createLogin(String value) {
    _authValue.update((val) {
      val?.createLogin = value;
    });
  }

  String get createPassword1 => _authValue.value.createPassword1;

  set createPassword1(String value) {
    _authValue.update((val) {
      val?.createPassword1 = value;
    });
  }

  String get createPassword2 => _authValue.value.createPassword2;

  set createPassword2(String value) {
    _authValue.update((val) {
      val?.createPassword2 = value;
    });
  }

  bool get rightPasswords {
    if (createPassword1 != createPassword2 ||
        createPassword1.isEmpty ||
        createPassword2.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

  bool get loginError => _authValue.value.loginError;

  set loginError(bool value) {
    _authValue.update((val) {
      val?.loginError = value;
    });
  }

  bool get passwordError => _authValue.value.passwordError;

  set passwordError(bool value) {
    _authValue.update((val) {
      val?.passwordError = value;
    });
  }

  bool get loginAuthError => _authValue.value.loginAuthError;

  set loginAuthError(bool value) {
    _authValue.update((val) {
      val?.loginAuthError = value;
    });
  }

  bool get passwordAuthError => _authValue.value.passwordAuthError;

  set passwordAuthError(bool value) {
    _authValue.update((val) {
      val?.passwordAuthError = value;
    });
  }

  String get accessToken => _authValue.value.accessToken;

  set accessToken(String value) {
    _authValue.update((val) {
      val?.accessToken = value;
    });
  }

  bool register() {
    if (loginError == false &&
        passwordError == false &&
        rightPasswords == true) {
      registerAuth();
      return true;
    } else if (loginError == false &&
        passwordError == false &&
        rightPasswords == false) {
      Get.snackbar("Пароли должны совпадать!", "Введенные пароли не совпадают",
          backgroundColor: Colors.yellow, colorText: Get.theme.primaryColor);
      return false;
    } else {
      Get.snackbar("Не все поля заполнены!", "Все поля должны быть заполнены!",
          backgroundColor: Colors.yellow, colorText: Get.theme.primaryColor);
      return false;
    }
  }

  void registerAuth() {
    login = createLogin;
    password = createPassword1.md5;
    Utils.showLoader();
    print(login);
    print(password);
    Get.find<RequestService>().signUp(login, password).then((value) {
      if (value) {
        Get.find<CryptService>().password = password;
        createLogin = "";
        createPassword1 = "";
        createPassword2 = "";
        Get.find<ProfileController>().isAuth = true;
        Get.find<StorageService>()
            .setValue(StorageEnum.login, login, needToEncrypt: true);
        Get.find<StorageService>()
            .setValue(StorageEnum.loginPassword, password, needToEncrypt: true);
        Get.find<StorageService>().setValue(StorageEnum.isAuth, true);
        Utils.closeLoader();
        Get.off(() => const ConfirmScreen());
        // Get.back();
        // Get.put(QuickController());
        // Get.to(() => const QuickSetScreen());
      } else {
        login = "";
        password = "";
        Utils.closeLoader();
      }
    });
  }

  void checkAuth() {
    print('login - $login');
    print('password - $password');
    if (loginAuthError == false && passwordAuthError == false) {
      Utils.showLoader();
      Get.find<RequestService>().signIn(login, password).then((value) {
        if (value.result) {
          Get.find<CryptService>().password = password;
          Get.find<ProfileController>().isAuth = true;
          Get.find<StorageService>()
              .setValue(StorageEnum.login, login, needToEncrypt: true);
          Get.find<StorageService>().setValue(
              StorageEnum.loginPassword, password,
              needToEncrypt: true);
          Get.find<StorageService>().setValue(StorageEnum.isAuth, true);
          Utils.closeLoader();

          if (Get.find<StorageService>().isQuick) {
            Get.off(() => const QuickCheckScreen());
          } else {
            Get.off(() => const QuickSetScreen());
          }
        } else if (value.result == false &&
            value.errorMessage == "Email not confirmed") {
          Get.off(() => const ConfirmScreen());
        } else {
          login = "";
          password = "";
          Utils.closeLoader();
        }
      });
    } else {
      Get.snackbar("Одно из полей введено некорректно",
          "Сначала корректно заполните поля",
          backgroundColor: Colors.yellow, colorText: Get.theme.primaryColor);
    }
  }
}
