import 'package:animate_do/animate_do.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';
import 'package:sgrc/blocks/auth/screens/registration_screen.dart';
import 'package:sgrc/blocks/auth/widgets/login_textfield/login_textfield.dart';
import 'package:sgrc/blocks/auth/widgets/password_textfield/password_textfield.dart';
import 'package:sgrc/blocks/main/screens/main_block.dart';
import 'package:sgrc/components/enums/auth_type.dart';
import 'package:sgrc/components/static/app_string.dart';
import 'package:sgrc/components/widgets/button.dart';
import 'package:sgrc/components/widgets/mospolytech.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: ListView(
          children: [
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 64),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Spin(
                    delay: const Duration(milliseconds: 500),
                    duration: const Duration(milliseconds: 3000),
                    infinite: true,
                    child: const Mospolytech(height: 125, width: 125),
                  ),
                  Column(
                    children: [
                      Container(
                          margin: const EdgeInsets.symmetric(vertical: 36),
                          child: const LoginTextField()),
                      const PasswordTextField(),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 36),
                    child: Column(
                      children: [
                        Button(
                            onPressed: () {
                              Get.find<AuthController>().checkAuth();
                            },
                            child: const Text(AppString.enter)),
                        Container(
                          margin: const EdgeInsets.only(top: 24),
                          child: RichText(
                              text: TextSpan(children: [
                            const TextSpan(
                                text: "${AppString.ifNotHaveAccount}, "),
                            TextSpan(
                                text: AppString.ifNotRegister,
                                style: const TextStyle(color: Colors.cyan),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Get.find<AuthController>()
                                        .changeAuth(AuthType.register);
                                  })
                          ])),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
