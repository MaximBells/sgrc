import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';
import 'package:sgrc/blocks/auth/screens/auth_block.dart';
import 'package:sgrc/blocks/profile/data/profile_controller.dart';
import 'package:sgrc/components/services/storage_service.dart';
import 'package:sgrc/components/widgets/button.dart';

class ConfirmScreen extends StatelessWidget {
  const ConfirmScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.find<StorageService>().removeAll();
          Get.find<ProfileController>().isAuth = false;
          Get.off(() => const AuthBlock());
        },
        child: const Icon(Icons.exit_to_app),
      ),
      body: Center(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 32),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 16),
                child: Text(
                  "Для использования приложения вам нужно подтвердить почту.",
                  maxLines: null,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 16),
                child: Text(
                  "Нажмите кнопку ниже, если вы успешно подтвердили почту",
                  maxLines: null,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 16),
                child: Button(
                    child: Text(
                      "Подтвердить",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    onPressed: () {
                      Get.find<AuthController>().confirm();
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
