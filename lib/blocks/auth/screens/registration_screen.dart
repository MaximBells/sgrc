import 'package:animate_do/animate_do.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';
import 'package:sgrc/blocks/auth/screens/login_screen.dart';
import 'package:sgrc/blocks/auth/widgets/login_textfield/login_textfield.dart';
import 'package:sgrc/blocks/auth/widgets/set_login/set_login.dart';
import 'package:sgrc/blocks/auth/widgets/set_password/set_password.dart';
import 'package:sgrc/components/enums/auth_type.dart';
import 'package:sgrc/components/static/app_string.dart';
import 'package:sgrc/components/widgets/button.dart';

class RegistrationScreen extends StatelessWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: ListView(
          children: [
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 48),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Container(
                          margin: const EdgeInsets.symmetric(vertical: 36),
                          child: const SetLogin(hintText: "Введите логин...")),
                      const SetPassword(
                        tag: '1',
                        hintText: "Придумайте пароль...",
                      ),
                      Container(
                          margin: const EdgeInsets.symmetric(vertical: 36),
                          child: const SetPassword(
                            tag: '2',
                            hintText: "Повторно введите пароль...",
                          )),
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 36),
                    child: Column(
                      children: [
                        Button(
                            onPressed: () {
                              Get.find<AuthController>().register();
                            },
                            child: const Text(AppString.register)),
                        Container(
                          margin: const EdgeInsets.only(top: 24),
                          child: RichText(
                              text: TextSpan(children: [
                            const TextSpan(
                                text: "${AppString.ifHaveAccount}, "),
                            TextSpan(
                                text: AppString.authorise,
                                style: const TextStyle(color: Colors.cyan),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Get.find<AuthController>()
                                        .changeAuth(AuthType.login);
                                  })
                          ])),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
