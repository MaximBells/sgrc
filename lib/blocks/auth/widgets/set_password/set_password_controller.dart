import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';

class SetPasswordValue {
  String text = "";
  String? errorText;
}

class SetPasswordController extends GetxController {
  final String tag;

  SetPasswordController({required this.tag});

  final TextEditingController textController = TextEditingController();

  final _setPasswordValue = SetPasswordValue().obs;

  String get text => _setPasswordValue.value.text;

  set text(String value) {
    _setPasswordValue.update((val) {
      val?.text = value;
    });
    update();
  }

  String? get errorText => _setPasswordValue.value.errorText;

  set errorText(String? value) {
    _setPasswordValue.update((val) {
      val?.errorText = value;
    });
    update();
  }

  void updateText(String value) {
    text = value;
    if (tag.contains('1')) {
      Get.find<AuthController>().createPassword1 = text;
    } else {
      Get.find<AuthController>().createPassword2 = text;
    }
    if (text.isEmpty) {
      errorText = "Пароль не должен быть пустым";
      Get.find<AuthController>().passwordError = true;
    } else if (text.length < 6) {
      errorText = "Пароль должен содержать не менее 6 символов";
      Get.find<AuthController>().passwordError = true;
    } else {
      errorText = null;
      Get.find<AuthController>().passwordError = false;
    }
  }
}
