import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';

import 'package:sgrc/blocks/auth/widgets/set_password/set_password_controller.dart';

class SetPassword extends StatelessWidget {
  const SetPassword({Key? key, this.hintText, required this.tag})
      : super(key: key);
  final String? hintText;
  final String tag;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SetPasswordController>(
        global: false,
        init: SetPasswordController(tag: tag),
        builder: (controller) {
          return TextField(
            onChanged: (text) {
              controller.updateText(text);

            },
            decoration: InputDecoration(
                errorText: controller.errorText,
                errorStyle: TextStyle(color: Colors.yellow),
                focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.yellow)),
                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.yellow)),
                hintText: hintText,
                border: const OutlineInputBorder()),
          );
        });
  }
}
