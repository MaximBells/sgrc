import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';
import 'package:sgrc/blocks/auth/widgets/password_textfield/password_textfield_controller.dart';
import 'package:sgrc/components/static/app_string.dart';

class PasswordTextField extends StatelessWidget {
  const PasswordTextField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PasswordTexFieldController>(
        init: PasswordTexFieldController(),
        global: false,
        builder: (controller) {
          return TextField(
            controller: controller.textController,
            obscureText: controller.hide,
            onChanged: (text) {
              controller.updateText(text);
            },
            decoration: InputDecoration(
                errorStyle: TextStyle(color: Colors.yellow),
                focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.yellow)),
                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.yellow)),
                helperText: AppString.passwordHelperText,
                helperMaxLines: 2,
                errorText: controller.errorText,
                hintText: AppString.password,
                border: const OutlineInputBorder(),
                suffixIcon: IconButton(
                    onPressed: () {
                      controller.hide = !controller.hide;
                    },
                    icon: controller.icon)),
          );
        });
  }
}
