import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';
import 'package:sgrc/components/extensions/string_extension.dart';

class PasswordTextFieldValue {
  bool hide = true;
  Widget icon = const Icon(Icons.remove_red_eye);
  String? errorText;
  String text = "";
}

class PasswordTexFieldController extends GetxController {
  final _passwordTextFieldValue = PasswordTextFieldValue().obs;

  final TextEditingController textController = TextEditingController();

  bool get hide => _passwordTextFieldValue.value.hide;

  set hide(bool value) {
    _passwordTextFieldValue.update((val) {
      val?.hide = value;
    });
    if (hide == true) {
      icon = const Icon(Icons.remove_red_eye_outlined);
    } else {
      icon = const Icon(Icons.remove_red_eye);
    }
    update();
  }

  Widget get icon => _passwordTextFieldValue.value.icon;

  set icon(Widget value) {
    _passwordTextFieldValue.update((val) {
      val?.icon = value;
    });
  }

  String? get errorText => _passwordTextFieldValue.value.errorText;

  set errorText(String? value) {
    _passwordTextFieldValue.update((val) {
      val?.errorText = value;
    });
    update();
  }

  String get text => _passwordTextFieldValue.value.text;

  set text(String value) {
    _passwordTextFieldValue.update((val) {
      val?.text = value;
    });
    update();
  }

  void updateText(String value) {
    text = value;
    if (text.isEmpty) {
      errorText = "Поле пароля не должно быть пустым";
      Get.find<AuthController>().passwordAuthError = true;
    } else if (text.length < 6) {
      errorText = "Длина пароля менее 6";
      Get.find<AuthController>().passwordAuthError = true;
    } else {
      Get.find<AuthController>().password = value.md5;
      errorText = null;
      Get.find<AuthController>().passwordAuthError = false;
    }
  }
}
