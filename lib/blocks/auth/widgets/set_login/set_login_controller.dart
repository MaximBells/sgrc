import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';

class SetLoginValue {
  String text = "";
  String? errorText;
}

class SetLoginController extends GetxController {
  final _setLoginValue = SetLoginValue().obs;

  String get text => _setLoginValue.value.text;

  set text(String value) {
    _setLoginValue.update((val) {
      val?.text = value;
    });
    update();
  }

  String? get errorText => _setLoginValue.value.errorText;

  set errorText(String? value) {
    _setLoginValue.update((val) {
      val?.errorText = value;
    });
    update();
  }

  void updateText(String value) {
    text = value;
    Get.find<AuthController>().createLogin = text;
    if (text.isEmpty) {
      errorText = "Логин не может быть пустым";
      Get.find<AuthController>().loginError = true;
    } else if (!GetUtils.isEmail(text)) {
      errorText = "Email введен некорректно";
      Get.find<AuthController>().loginError = true;
    } else {
      Get.find<AuthController>().loginError = false;
      errorText = null;
    }
  }
}
