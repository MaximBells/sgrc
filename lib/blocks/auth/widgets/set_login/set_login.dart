import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';
import 'package:sgrc/blocks/auth/widgets/set_login/set_login_controller.dart';

class SetLogin extends StatelessWidget {
  const SetLogin({Key? key, required this.hintText}) : super(key: key);
  final String hintText;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SetLoginController>(
        global: false,
        init: SetLoginController(),
        builder: (controller) {
          return TextField(
              onChanged: (text) {
                controller.updateText(text);
              },
              decoration: InputDecoration(
                  errorText: controller.errorText,
                  errorStyle: TextStyle(color: Colors.yellow),
                  focusedErrorBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.yellow)),
                  errorBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.yellow)),
                  hintText: hintText,
                  border: const OutlineInputBorder()));
        });
  }
}
