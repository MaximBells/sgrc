import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';

class LoginValue {
  String? errorText;
  String text = "";
}

class LoginTextFieldController extends GetxController {
  final TextEditingController textController = TextEditingController();

  final _loginValue = LoginValue().obs;

  String? get errorText => _loginValue.value.errorText;

  set errorText(String? value) {
    _loginValue.update((val) {
      val?.errorText = value;
    });
    update();
  }

  String get text => _loginValue.value.text;

  set text(String value) {
    _loginValue.update((val) {
      val?.text = value;
    });
    update();
  }

  void updateText(String value) {
    text = value;
    if (text.isEmpty) {
      errorText = "Поле логина не должно быть пустым";
      Get.find<AuthController>().loginAuthError = true;
    } else if (!GetUtils.isEmail(text)) {
      errorText = "Логин введен некорректно";
      Get.find<AuthController>().loginAuthError = true;
    } else {
      Get.find<AuthController>().login = text;
      errorText = null;
      Get.find<AuthController>().loginAuthError = false;
    }
  }
}
