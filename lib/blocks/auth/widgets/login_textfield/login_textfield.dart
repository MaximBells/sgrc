import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/auth/data/auth_controller.dart';
import 'package:sgrc/blocks/auth/widgets/login_textfield/login_textfield_controller.dart';
import 'package:sgrc/components/static/app_string.dart';

class LoginTextField extends StatelessWidget {
  const LoginTextField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginTextFieldController>(
        init: LoginTextFieldController(),
        global: false,
        builder: (controller) {
          return TextField(
            controller: controller.textController,
            decoration: InputDecoration(
                errorStyle: TextStyle(color: Colors.yellow),
                focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.yellow)),
                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.yellow)),
                helperText: AppString.loginHelperText,
                errorText: controller.errorText,
                border: OutlineInputBorder(),
                hintText: AppString.login),
            onChanged: (text) {
              controller.updateText(text);
            },
          );
        });
  }
}
