import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/main/screens/graphic_screen.dart';
import 'package:sgrc/blocks/main/data/main_controller.dart';
import 'package:sgrc/components/services/storage_service.dart';

class MainBlock extends GetView<MainController> {
  const MainBlock({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          body: AnimatedSwitcher(
            duration: const Duration(milliseconds: 500),
            child: controller.activeScreen,
          ),
          bottomNavigationBar: BottomNavyBar(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            selectedIndex: controller.activeIndex,
            onItemSelected: (index) {
              controller.activeIndex = index;
              controller.activeScreen =
                  controller.screens[controller.activeIndex];
            },
            items: <BottomNavyBarItem>[
              BottomNavyBarItem(
                  title: Text('События'), icon: Icon(Icons.whatshot)),
              BottomNavyBarItem(
                  title: Text('Профиль'), icon: Icon(Icons.person)),
            ],
          ),
        ));
  }
}
