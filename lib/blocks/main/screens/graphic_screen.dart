import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/main/data/main_controller.dart';
import 'package:sgrc/components/widgets/mospolytech.dart';

class GraphicScreen extends GetView<MainController> {
  const GraphicScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 34, vertical: 34),
            child: Spin(
              delay: const Duration(milliseconds: 500),
              duration: const Duration(milliseconds: 2000),
              infinite: true,
              child: const Mospolytech(height: 150, width: 150),
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 34, vertical: 12),
            child: const Text(
              "Данный раздел находится в разработке",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }
}
