import 'dart:async';

import 'package:candlesticks/candlesticks.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sgrc/blocks/main/screens/graphic_screen.dart';
import 'package:sgrc/blocks/profile/screens/profile_block.dart';
import 'package:sgrc/components/services/request_service.dart';

class MainValue {
  Widget activeScreen = Container();
  List<Widget> screens = [const GraphicScreen(), const ProfileBlock()];
  int activeIndex = 1;
}

class MainController extends GetxController {


  @override
  void onInit() {
    super.onInit();
    activeScreen = screens[activeIndex];
  }

  final _mainValue = MainValue().obs;

  final PageController pageController = PageController();

  Widget get activeScreen => _mainValue.value.activeScreen;

  set activeScreen(Widget value) {
    _mainValue.update((val) {
      val?.activeScreen = value;
    });
  }

  List<Widget> get screens => _mainValue.value.screens;

  set screens(List<Widget> value) {
    _mainValue.update((val) {
      val?.screens = value;
    });
  }

  int get activeIndex => _mainValue.value.activeIndex;

  set activeIndex(int value) {
    _mainValue.update((val) {
      val?.activeIndex = value;
    });
  }
}
